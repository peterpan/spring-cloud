package com.cloud.controller;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by ICE on 2016/12/10.
 */
@FeignClient(value = "SPRING-CLOUD-PROVIDER",fallback = ComputeHystrix.class)
public interface ComputeClient {

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String add(@RequestParam(name="a") Integer a, @RequestParam(name="b") Integer b);
}
