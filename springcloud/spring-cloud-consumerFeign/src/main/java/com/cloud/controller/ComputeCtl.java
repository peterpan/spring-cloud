package com.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ICE on 2016/12/10.
 */
@RestController
public class ComputeCtl {

    @Autowired
    ComputeClient computeClient;

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String add(@RequestParam(name="a") Integer a, @RequestParam(name="b") Integer b){
        return computeClient.add(a,b);
    }
}
