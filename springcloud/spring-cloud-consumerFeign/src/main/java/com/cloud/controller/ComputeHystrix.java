package com.cloud.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by ICE on 2016/12/10.
 */
@Component
public class ComputeHystrix implements ComputeClient {
    @Override
    public String add(@RequestParam(name = "a") Integer a, @RequestParam(name = "b") Integer b) {
        return "-9999";
    }
}
