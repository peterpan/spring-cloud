package com.cloud.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

/**
 * Created by ICE on 2016/12/10.
 */
@Service
public class ComputeService {

    @Autowired
    private RestTemplate restTemplate;

   @HystrixCommand(fallbackMethod = "computeFallback")
    public String add(Integer a, Integer b){
        return restTemplate.getForEntity("http://SPRING-CLOUD-PROVIDER/add?a="+a+"&b="+b,String.class).getBody();
    }

    public String computeFallback(){
        return "error";
    }
}
