package com.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ICE on 2016/12/10.
 */
@RestController
public class ComputeCtrl {

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public Integer add(@RequestParam(name="a") Integer a,@RequestParam(name="b") Integer b){
        ServiceInstance instance = discoveryClient.getLocalServiceInstance();
        Integer r = a + b;
        System.out.println(String.format("host:%s,port:%s,serviceId:%s,result:%s",instance.getHost(),instance.getPort(),instance.getServiceId(),r));
        return r;

    }
    @Value("${test}")
    private String test;

    @RequestMapping("/test")
    public String test(){
        return this.test;
    }
}
